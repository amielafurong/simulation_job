folder( "first-try" ){

}

def folder = "first-try"
def generatejob = folder + "/initial-job"


     freeStyleJob(generatejob)
	{

	steps {
	 scm {
		
        git{
		remote{
				url('https://github.com/amielafurong/sample.git')
				credentials('2fc542b3-e1e7-41bf-9bfe-2c085733ec49')
			}
			}
    }
	
	
	maven {
		mavenInstallation('maven_home')
		goals('package')
	}
	
	publishers {
        downstreamParameterized {
            trigger('Nexus') {
                condition('SUCCESS')
                parameters {
                      predefinedProp('CUSTOM_WORKSPACE', '$WORKSPACE')
					  predefinedProp('CUSTOM_BUILD_ID', '$BUILD_ID')

                }
            }
        }
    }

	} 
}
	
	def job2 = folder + "/Nexus"
	freeStyleJob(job2)
	{
	steps{
			
			
				parameters {
				stringParam('CUSTOM_BUILD_ID')
				stringParam('CUSTOM_WORKSPACE')
				}
		}
	}